import os
import multiprocessing as mpr
import time
import logging
import random

# from pymodbus.client.sync import ModbusSerialClient as ModbusClient


FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
_log = logging.getLogger()
_log.setLevel(logging.DEBUG)


lock = mpr.Lock()
counter = mpr.Value('i', 0)
number = mpr.Value('i', 0)

def proc(nr, q):
    while True:
        lock.acquire()
        try:
            with number.get_lock():
                number.value = nr
            s = "parent: " + str(os.getppid()) + " pid: " + str(os.getpid()) + " "
            q.put([s, number.value, ": ", counter.value, ' ', random.randint(0,9)])
            with counter.get_lock():
                counter.value += 1
        finally:
            lock.release()
            time.sleep(0.5)

def getting(q):
    while True:
        print(q.get())

if __name__ == '__main__':
    q = mpr.Queue()

    p1 = mpr.Process(target=proc, args=(1, q))
    p2 = mpr.Process(target=proc, args=(2, q))
    p3 = mpr.Process(target=proc, args=(3, q))
    p_get = mpr.Process(target=getting, args=(q,))

    p1.start()
    p2.start()
    p3.start()
    p_get.start()

    p1.join()
    p2.join()
    p3.join()
    p_get.join()
